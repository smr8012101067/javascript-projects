// Capture the current theme from local storage and adjust the page to use the current theme.
const htmlEl = document.getElementsByTagName("html")[0];
console.log(htmlEl);

const currentTheme = localStorage.getItem("theme1")
  ? localStorage.getItem("theme1")
  : null;
console.log(currentTheme);

if (currentTheme) {
  htmlEl.dataset.theme1112 = currentTheme;
}

// When the user changes the theme, we need to save the new value on local storage
const toggleTheme = (theme2) => {
  console.log(theme2);
  htmlEl.dataset.theme1112 = theme2;
  localStorage.setItem("theme1", theme2);
};
